function addTokens(input, tokens){
   if (typeof input != "string")
	    throw TypeError("Invalid input");
	
	if(input.length<6)
	    throw TypeError("Input should have at least 6 characters");
	  
	for(let i=0; i<tokens.length;i++)    
		if(typeof (tokens[0].tokenName) !== 'string')    
	    	throw TypeError("Invalid array format");
	     
	    
	  
	if(!input.includes("..."))
		return input;
	else
		{
		for(let i=0; i<tokens.length;i++)
			input = input.replace('...', '${' + tokens[i].tokenName + '}');
	
		return input;
		} 
}

const app = {
    addTokens: addTokens
}

module.exports = app;